
## Unsupervised Learning of Consensus Maximization for 3D Vision Problems
Thomas Probst, Danda Pani Paudel, Ajad Chhatkuli, Luc Van Gool

### Introduction
This work is based on our [CVPR19 paper](http://openaccess.thecvf.com/content_CVPR_2019/papers/Probst_Unsupervised_Learning_of_Consensus_Maximization_for_3D_Vision_Problems_CVPR_2019_paper.pdf) and demonstrates the basic unsupervised learning strategy.
It contains an implementation of the Vandermonde loss function for fundamental matrix estimatiom and a minimal training example on synthetic [ModelNet](http://modelnet.cs.princeton.edu/) data.



### Citation
If you find our work useful in your research, please consider citing:

    @inproceedings{Probst2019UnsupervisedLO,
      title={Unsupervised Learning of Consensus Maximization for 3D Vision Problems},
      author={Thomas Probst and Danda Pani Paudel and Ajad Chhatkuli and Luc Van Gool},
      booktitle={CVPR 2019},
      year={2019}
    }
   
### Installation
Using [anaconda](https://www.anaconda.com/distribution/) package manager.

    git clone git@bitbucket.org:probstt/ulcm-public.git
    cd ulcm-public

    conda create -n ulcm python=3.6 tensorflow-gpu=1.10 scipy 
    conda activate ulcm

### Usage
To train a model on the provided fundamental matrix dataset.

    python train_2d2d.py --dataset=data/fundamental --supervised=False 

We can use TensorBoard to view the network architecture and monitor the training progress.

    tensorboard --logdir log/exp

### License
The code is based on the tensorflow [PointNet implementation](https://github.com/charlesq34/pointnet) and is released under MIT License (see LICENSE file for details).
