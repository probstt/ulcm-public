function [R2,t2,evar] = simCamTransform(trtype,X)

evar = '';
N = size(X,2);
if strcmpi(trtype, 'full')    
    R2 = eul2rotm(0.8* (0.5 - rand(1,3)));
    X2 = R2*X;
    t2 = mean(X-X2,2);
    t2 = [t2(1:2);0] + [0.5;0.5;1].*(0.5-rand(3,1));
elseif strcmpi(trtype, 'rotation')    
    R2 = eul2rotm(0.8* (0.5 - rand(1,3)));
    t2 = zeros(3,1);
elseif strcmpi(trtype, 'translation')
    R2 = eul2rotm(0.6* (0.5 - rand(1,3)));
    X2 = R2*X;
    t2 = mean(X-X2,2);
    t2 = [t2(1:2);0] + [0.5;0.5;1].*(0.5-rand(3,1));
    R2 = eye(3);    
elseif strcmpi(trtype, 'rot_single')
    rotvar = randi([0,2]);
    if rotvar == 0
        R2 = rotx(180/pi*0.8*(0.5- rand(1,1)));
        evar = 'X';
    elseif rotvar == 1
        R2 = roty(180/pi*0.8*(0.5- rand(1,1)));
        evar = 'Y';
    else
        R2 = rotz(180/pi*0.8*(0.5- rand(1,1)));
        evar = 'Z';
    end
    t2 = zeros(3,1);
elseif strcmpi(trtype, 'tr_single')
    R2 = eul2rotm(0.6* (0.5 - rand(1,3)));
    X2 = R2*X;
    t2 = mean(X-X2,2);
%     t2 = zeros(3,1);
    t2 = [t2(1:2);0] + [0.5;0.5;1].*(0.5-rand(3,1));
    trvar = randi([0,2]);
    if trvar == 0
        t2 = [t2(1);0;0];
        evar = 'X';
    elseif trvar == 1
        t2 = [0;t2(2);0];
        evar = 'Y';
    else
        t2 = [0;0;t2(3)];
        evar = 'Z';
    end
    R2 = eye(3);
end


end

