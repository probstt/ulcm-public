function [x_out,T]=normalize(x,alpha)
t=mean(x,2);
x_out=x-t;

x_out(1,:)=x_out(1,:)/alpha;

d1=mean(sqrt(sum(x_out.^2,1)));
s=sqrt(2)/d1;
x_out=x_out*s;


T=diag([s s 1])*[1/alpha 0 -t(1)/alpha;0 1 -t(2); 0 0 1];

%sum(sum(abs([x_out;ones(1,1917)]-T*[x;ones(1,1917)])))

