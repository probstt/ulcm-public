function [pts1,pts2, K] = simCamProj(camtype, motion, X, R, t)

% X=rand(3,N)+repmat([1 1 2]',1,N); % random points
N = size(X,2);
if strcmpi(camtype, 'perspective')
    P1=[eye(3)  zeros(3,1)];
    P2=[R' -R'*t];

    lx1=P1*[X;ones(1,N)] ; % project points in 1st camera
    lx2=P2*[X;ones(1,N)] ; % project points in second camera
    x1=lx1(1:2,:)./repmat(lx1(3,:),2,1);
    x2=lx2(1:2,:)./repmat(lx2(3,:),2,1);
    K=eye(3);
    
elseif strcmpi(camtype, 'orthographic') || strcmpi(camtype,'ortho')
    P1=eye(3);
    P1 = P1(1:2,:);
    
    P2 = R;
    P2 = P2(1:2,:);    
    t = t(1:2);
    
    x1=P1*X ; % project points in 1st camera
    x2=P2*X + t ; % project points in second camera   
    K=eye(3);
    
elseif strcmpi(camtype, 'weakperspective')
    s = 0.25 + 5*rand(1,1); % random scaling        
    A = [s 0;0 s];
    Pa1 = [eye(3) zeros(3,1)];
    Pa2 = [R' -R'*t];
        
    P1 = A*Pa1(1:2,:);
    P2 = A*Pa2(1:2,:);
    
    t = t(1:2,:);
    x1 = P1*[X;ones(1,N)];
    x2 = P2*[X;ones(1,N)];   
    K=eye(3);

elseif strcmpi(camtype, 'uncalibrated')
    f = 0.25 + 5*rand(1,1);    
    P1=[eye(3)  zeros(3,1)];
    P2=[R' -R'*t];       
    lx1=P1*[X;ones(1,N)] ; % project points in 1st camera
    lx2=P2*[X;ones(1,N)] ; % project points in second camera
    x1=f*lx1(1:2,:)./repmat(lx1(3,:),2,1);
    x2=f*lx2(1:2,:)./repmat(lx2(3,:),2,1);    
    s = f;
    K=eye(3);K(1,1)=f;K(2,2)=f;
    
    
elseif strcmpi(camtype, 'affine')        
    s = 0.25 + 5*rand(1,1); % random scaling
    a1 = s + 2*rand(1,1);
    a2 = s + 2*rand(1,1);
    xi = 0.1*rand(1,1);
    A = [a1 xi;0 a2];
    Pa1 = [eye(3) zeros(3,1)];
    Pa2 = [R' -R'*t];
        
    P1 = A*Pa1(1:2,:);
    P2 = A*Pa2(1:2,:);
    
    t = t(1:2,:);
    x1 = P1*[X;ones(1,N)];
    x2 = P2*[X;ones(1,N)];        
    K=A;
end

pts1 = x1;
pts2 = x2;

end