rng(42);
close all;
path_to_modelnet_h5='../../pointnet/data/modelnet40_ply_hdf5_2048/ply_data_test0.h5';

%% read modelnet data
data=hdf5read(path_to_modelnet_h5,'data');
NptsMax=size(data,2);
nModels=size(data,3);

%%

dataFolder='modelnet';homographyProbability=0;
% dataFolder='modelnet_homography';homographyProbability=1;
% dataFolder='modelnet_mixed';homographyProbability=0.5;

dataFolder=['../data/' dataFolder];
mkdir(dataFolder)

npts=512;
nmodels=512;
model_selection=randperm(nModels,nmodels);
outlierRatio=[linspace(0.1,0.9,9)];

%%
for outlierRate=outlierRatio
    outlierRate
    
    didx=1;
    dataset={};
    for i=model_selection
        
        %sample pts
        X=data(:,randperm(NptsMax,npts),i);
        %X=0.5- rand(3,npts);
        
        %randomly rotate X
        angles=0.8 * (0.5 - rand(1,3));
        Rglobal=eul2rotm(angles);
        X=Rglobal*X;
        
        %put in front of camera 1
        X=X+repmat([0 0 2]',1,npts);
        
        
        %mix homographies
        doHomography=(rand<=homographyProbability);
    
        if doHomography
            motion = 'rotation';
        else
            motion = 'full';
        end
        
        camproj = 'uncalibrated';
        
        %generate motion
        [R,t,~] = simCamTransform(motion, X);
        
        % generate camera projections
        [x1p_org,x2p_org,K] = simCamProj(camproj,motion,X,R,t);
        [x1p,TN1]=normalize(x1p_org,1);
        [x2p,TN2]=normalize(x2p_org,1);     
%         TN1=eye(3);TN2=eye(3);
        
        x1p(3,:)=1;
        x2p(3,:)=1;
        
        if ~doHomography
            %Compute fundamental matrix from E
            E=[0 -t(3) t(2); t(3) 0 -t(1); -t(2) t(1) 0]*R;
            E=E/E(end);
            
            Forg=inv(K')*E*inv(K);
            Forg=Forg'/Forg(end);

            Fn=inv(TN2')*Forg*inv(TN1);
            Fn=Fn/norm(Fn);
            
            H=eye(3);
        else
            
            %Compute homography from rotation
            H=K*R*inv(K);
            Hn=TN2*inv(H)*inv(TN1);
            

            E=eye(3);
            Forg=eye(3);
            Fn=eye(3);
        end

        %add noise
        noise =6/512;
        x1p(1:2,:)=x1p(1:2,:)+(2*rand(2,npts)-1)*noise;
        x2p(1:2,:)=x2p(1:2,:)+(2*rand(2,npts)-1)*noise;



        
        %% Generate outliers by mixing
        matches=[1:npts;1:npts];
        noutliers=round(npts*outlierRate);
        outlierInds=randperm(npts,noutliers);
        matches(2,outlierInds)=randperm(npts,noutliers);
        
        %% remove confusing outliers
        if doHomography
            x1p_2=Hn*x1p(:,matches(1,outlierInds));
            x1p_2=x1p_2./x1p_2(3,:);
            dists=sqrt(sum((x2p(:,matches(2,outlierInds))-x1p_2).^2,1));
            inlierThresh=0.08;
        else
            dists=abs(sum(x2p(:,matches(2,outlierInds)).*(Fn*x1p(:,matches(1,outlierInds))),1));
            inlierThresh=0.08;
        end
        
        notOutliers=(dists<=inlierThresh);
        matches(2,outlierInds(notOutliers))=matches(1,outlierInds(notOutliers));
        outlierInds=outlierInds(~notOutliers);

        
        %arrange pts according to matches
        x1p=x1p(:,matches(1,:));
        x2p=x2p(:,matches(2,:));
        labels=zeros(1,npts);labels(outlierInds)=1;
        
        
        %save data
        dataset(didx).x1p=x1p;
        dataset(didx).x2p=x2p;
        dataset(didx).labels=labels;
        dataset(didx).TN1=TN1;
        dataset(didx).TN2=TN2;
        dataset(didx).F=Forg;
        dataset(didx).Fn=Fn;
        dataset(didx).E=E;
        dataset(didx).K=K;
        dataset(didx).R=R;
        dataset(didx).t=t;
        dataset(didx).H=H;
        didx=didx+1;

        
    end
    
    fname=sprintf('%s/match2d2d_no_%02d.mat',dataFolder,uint8(outlierRate*100));
    save(fname,'dataset','outlierRate','-v6');
    display([fname ' exported.']);
end


