import tensorflow as tf
import numpy as np
import math
import sys
import os
import pdb

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)
sys.path.append(os.path.join(BASE_DIR, '../utils'))
import tf_util
from transform_nets import input_transform_net, feature_transform_net


def placeholder_inputs_2d2d(batch_size, num_point, inputDim=6,vanderDim=7):
    pointclouds_pl = tf.placeholder(tf.float32,
                                     shape=(batch_size, num_point, inputDim))
    labels_pl = tf.placeholder(tf.float32,
                                shape=(batch_size, num_point))
    mask_pl = tf.placeholder(tf.float32,
                                shape=(batch_size, num_point))

    vander_pl = tf.placeholder(tf.float32,
                             shape=(batch_size, num_point,vanderDim))

    fund_pl = tf.placeholder(tf.float32,
                             shape=(batch_size, 9))

    motiontype_pl = tf.placeholder(tf.float32,
                             shape=(batch_size,1))

    return pointclouds_pl, labels_pl, mask_pl,vander_pl, fund_pl, motiontype_pl

def get_model(point_cloud, is_training, bn_decay=None,inputDim=6):
    """ Classification PointNet, input is BxNxinputDim, output BxNx1 """

    batch_size = tf.shape(point_cloud)[0]
    num_point = tf.shape(point_cloud)[1]
    end_points = {}

    print(point_cloud)

    net = tf.expand_dims(point_cloud, 2)
    with tf.variable_scope('transform_net1') as sc:
        transform = feature_transform_net(net, is_training, bn_decay, K=inputDim)
        print(transform)
    end_points['transform1'] = transform
    net_transformed = tf.matmul(tf.squeeze(net, axis=[2]), transform)
    net=tf.expand_dims(net_transformed,-1)

    print(net)


    net = tf_util.conv2d(net, 16, [1,inputDim],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv1', bn_decay=bn_decay)

    net = tf_util.conv2d(net, 64, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv2', bn_decay=bn_decay)

    net = tf_util.conv2d(net, 64, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv2_2', bn_decay=bn_decay)


    with tf.variable_scope('transform_net2') as sc:
        transform = feature_transform_net(net, is_training, bn_decay, K=64)
    end_points['transform2'] = transform
    net_transformed = tf.matmul(tf.squeeze(net, axis=[2]), transform)
    point_feat = tf.expand_dims(net_transformed, [2])
    print(point_feat)

    net = tf_util.conv2d(point_feat, 64, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv3', bn_decay=bn_decay)
    net = tf_util.conv2d(net, 128, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv4', bn_decay=bn_decay)
    net = tf_util.conv2d(net, 1024, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv5', bn_decay=bn_decay)

    global_feat=tf.reduce_max(net,axis=1,keep_dims=True)
    print(global_feat)

    global_feat_expand = tf.tile(global_feat, [1, num_point, 1, 1])
    concat_feat = tf.concat((point_feat, global_feat_expand),axis=3)
    print(concat_feat)

    net = tf_util.conv2d(concat_feat, 512, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv6', bn_decay=bn_decay)
    net = tf_util.conv2d(net, 256, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv7', bn_decay=bn_decay)

    net = tf_util.conv2d(net, 128, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv8', bn_decay=bn_decay)
    net = tf_util.conv2d(net, 64, [1,1],
                         padding='VALID', stride=[1,1],
                         bn=True, is_training=is_training,
                         scope='conv9', bn_decay=bn_decay)

    net = tf_util.conv2d(net, 1, [1,1],
                         padding='VALID', stride=[1,1], activation_fn=None,
                         scope='conv10')

    net = tf.squeeze(net) # BxN
    net=tf.reshape(net,[batch_size,num_point])

    return net, end_points

##

#Loss penalizes the last singular values => inliers should give good nullspace
def vanderLoss(vander,N,B,transform='rigid',lambda_ortho=0.01,sigmaWeights=(1,1,1)):

    vanderDim = int(vander.get_shape()[2])
    Nf=tf.cast(N,dtype=tf.float32)

    if transform=='rigid' or transform == 'similarity':

        s,u,v=tf.svd(vander,compute_uv=True)

        #Get 3 Nullvectors: x1 x2 x3 y1 y2 y3 1
        NV = v[:,:, vanderDim-3:vanderDim]

        #Get 3x3 coefficients for y1 y2 y3 for 3 SVs
        A = NV[:,3:3+3,:]

        #Compute disentangled orthogonal basis
        OB=-tf.matrix_inverse(A+10e-9*tf.eye(3, 3, batch_shape=[B]))

        #Change of bases
        NVc = tf.matmul(NV,OB)

        #Extract rotation matrices
        Rest = tf.transpose(NVc[:,0:3,:],perm=[0, 2, 1])

        #orthogonality constraint
        RR=tf.matmul(Rest,Rest,transpose_b=True)
        I = tf.eye(3, 3, batch_shape=[B])

        if transform == 'rigid':
            ortho_loss = tf.reduce_mean(tf.log(tf.norm(tf.reshape(RR - I, (B, -1)), ord='euclidean', axis=1)))
            reflexion_loss = tf.constant(0, dtype=tf.float32)

        else: #similarity
            scale = tf.pow(tf.square(tf.tile(tf.reshape(tf.linalg.det(Rest), (B, 1,1)),[1,3,3])),1.0/3.0)
            ortho_loss = tf.reduce_mean(tf.log(tf.norm(tf.reshape(RR - scale*I, (B, -1)), ord='euclidean', axis=1)))
            #ortho_loss = tf.reduce_mean(tf.norm(tf.reshape(RR - scale * I, (B, -1)), ord='euclidean', axis=1))

            reflexion_loss=tf.reduce_mean(tf.square(1-tf.linalg.det(Rest/tf.sqrt(scale))))
            tf.summary.scalar('reflexion_loss', reflexion_loss)

        tf.summary.scalar('ortho_loss', ortho_loss)


    elif transform=='affine':
        s = tf.svd(vander, compute_uv=False)
        ortho_loss=tf.constant(0,dtype=tf.float32)
        reflexion_loss = tf.constant(0, dtype=tf.float32)
    else:
        print('NO VALID TRANSFORM!')

    nSigmas=len(sigmaWeights)
    lastSVs=s[:,vanderDim-nSigmas:vanderDim]
    print('nSigmas: {}'.format(nSigmas))

    # tf_sigmaWeights = tf.constant(np.asarray(sigmaWeights), dtype=tf.float32)
    # lastSVs=lastSVs*tf_sigmaWeights

    vander_loss = tf.reduce_mean(lastSVs, axis=1)
    vander_loss=tf.reduce_mean(vander_loss)#*512/Nf

    tf.summary.scalar('vander_loss', vander_loss)


    return vander_loss+ortho_loss*lambda_ortho+reflexion_loss*lambda_ortho

##################################################
# 2d-2d
##################################################

def get_loss_2d2d(pred, label, mask, vander,supervised=False,inlierLossLambda=0.2,doHomography=False):
    """ pred: BxNxC,
        label: BxN, """
    B=int(pred.shape[0])
    N= int(pred.shape[1])
    loss = tf.nn.sigmoid_cross_entropy_with_logits(logits=pred, labels=label)
    print(loss)
    loss=loss*mask
    classify_loss = tf.reduce_mean(loss)
    tf.summary.scalar('classify loss', classify_loss)


    #Compute SVD on weighted vandermonde
    vanderDim = int(vander.shape[2])

    #soft threshold inliers
    inlierThresh=0.5
    inlierProb=1.0-tf.sigmoid(pred)

    #inlierCountSoft=tf.nn.softplus(inlierProb-inlierThresh)*2
    inlierCountSoft = tf.nn.sigmoid(5*(inlierProb - inlierThresh))
    inlierCountSoft=tf.reduce_sum(inlierCountSoft,axis=1)
    tf.summary.scalar('inliersSoft', tf.reduce_mean(inlierCountSoft))

    nInliers = tf.reduce_sum(tf.cast(tf.greater_equal(inlierProb, inlierThresh), tf.float32), axis=1)
    tf.summary.scalar('inliers', tf.reduce_mean(nInliers))

    weights = inlierProb
    #weights = tf.nn.relu(inlierProb-inlierThresh)*2
    #weights=weights*tf.tile(tf.reshape(vander_loss_active,(-1,1)),(1,N))

    #add small random values to produce high vander loss in case of too few inliers
    weights=weights+tf.random_uniform((B,N),0,1e-9)

    #normalize weights: sum up to 1 across all points
    weights=weights/(tf.tile(tf.reshape(tf.norm(weights,axis=1),(-1,1)),(1,N)))

    #match dimension of vandermonde matrix
    weights=tf.tile(tf.reshape(weights,(B,N,1)),(1,1,vanderDim))

    #weight vandermonde matrix
    weightedVander=weights*vander

    if doHomography:
        vander_loss = vanderLoss(weightedVander, N, B, transform='affine',sigmaWeights=(1,1,1))
    else:
        vander_loss = vanderLossFundamental(weightedVander, N, B)


    #tf.summary.scalar('vander loss', vander_loss)


    inlierRatioSoft = tf.reduce_mean(inlierCountSoft / np.float(N))



    inlier_loss=-tf.reduce_mean(inlierRatioSoft)

    tf.summary.scalar('inlier loss', inlier_loss)


    loss = vander_loss + inlierLossLambda * inlier_loss

    if supervised:
        return classify_loss
    else:

        return loss



#Loss penalizes the last singular values => inliers should give good nullspace
def vanderLossFundamental(vander,N,B,lambda_ortho=0):

    vanderDim = int(vander.get_shape()[2])
    Nf=tf.cast(N,dtype=tf.float32)

    s, u, v = tf.svd(vander, compute_uv=True)

    F=v[:,:,vanderDim-1:vanderDim]

    ortho_loss=tf.abs(tf.linalg.det(tf.reshape(F,(B,3,3))))
    ortho_loss=tf.reduce_mean(ortho_loss)

    lastSVs=s[:,vanderDim-1:vanderDim]


    vander_loss = tf.reduce_mean(lastSVs, axis=1)
    vander_loss=tf.reduce_mean(vander_loss)#*512/Nf

    tf.summary.scalar('vander_loss', vander_loss)
    tf.summary.scalar('ortho_loss', ortho_loss)


    return vander_loss+ortho_loss*lambda_ortho

if __name__=='__main__':
    with tf.Graph().as_default():
        inputs = tf.zeros((32,1024,3))
        outputs = get_model(inputs, tf.constant(True))
        print(outputs)
