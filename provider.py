import random
import os
import sys
import numpy as np
import pdb
import glob, os
from scipy.io import loadmat,savemat

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)


def vandermonde(X):

    nVariables = X.shape[0]
    N=X.shape[1]

    if nVariables==4:
        combinations=np.asarray(
            ((1,     0,     1,     0), #xu
             (1,     0,     0,     1), #xv
             (1,     0,     0,     0), #x
             (0,     1,     1,     0), #yu
             (0,     1,     0,     1), #yv
             (0,     1,     0,     0), #y
             (0,     0,     1,     0), #u
             (0,     0,     0,     1)))#v
    elif nVariables==6:
        combinations=np.asarray((
             (1, 0, 0, 0, 0, 0),
             (0, 1, 0, 0, 0, 0),
             (0, 0, 1, 0, 0, 0),
             (0, 0, 0, 1, 0, 0),
             (0, 0, 0, 0, 1, 0),
             (0, 0, 0, 0, 0, 1),
             ))

    combinationsRow = combinations > 0

    #construct Vandermonde matrix V
    nTerms = combinationsRow.shape[0] + 1
    V=np.zeros((N,nTerms))
    for ti in np.arange(0,nTerms-1):
        dimIdx=np.where(combinationsRow[ti,:])[0]
        tval=X[dimIdx,:]
        degrees=combinations[ti,dimIdx]
        for r in  np.arange(1,tval.shape[0]):
            tval[r,:]=np.power(tval[r,:],degrees[r])

        tval=np.prod(tval,axis=0)
        V[:,ti]=tval

    V[:,nTerms-1]=1

    V=np.divide(V,np.linalg.norm(V,axis=1).reshape((-1,1)))

    return V


def prepare_2d2d_prematched(dataset):

    batchSize=dataset['x1p'].shape[0]
    nPoints=dataset['x1p'].shape[1]
    batch_out = np.zeros((batchSize,nPoints,4), dtype=np.float32)
    current_targets=np.zeros((batchSize,nPoints),dtype=np.float32)
    current_mask = np.ones((batchSize, nPoints), dtype=np.float32)
    current_vander = np.zeros((batchSize, nPoints,9), dtype=np.float32)

    for k in range(batchSize):

        x1p=np.copy(dataset['x1p'][k,:,0:2])
        x2p = np.copy(dataset['x2p'][k,:,0:2])
        labels=np.copy(dataset['labels'][k])

        #features for 4d input
        point_wise_translation = x2p - x1p

        #Vandermonde
        X=np.concatenate((x1p,x2p),axis=1)
        V=vandermonde(X.transpose())

        #construct data for network
        batch_out[k,:,:]=np.concatenate((x1p, point_wise_translation),axis=1)
        current_targets[k]=labels
        current_vander[k,:,:]=V

    return batch_out,current_targets,current_mask,current_vander


#Load mat files with matches
def modelnet_matches_from_mat_2d2d(mat_folder):

    matFiles = glob.glob(mat_folder +"/*.mat")
    if len(matFiles)==0:
        print('No mat file found.')
        exit(0)

    dataset=dict()
    names=list()
    idx=1
    for file in sorted(matFiles):
        corr_data=loadmat(file,matlab_compatible=True)

        if 'outlierRate' in corr_data:
            name='or_{:02d}'.format(np.int(np.floor(100*corr_data['outlierRate'][0,0])))
        elif 'noise' in corr_data:
            name = 'noise_{:d}'.format(np.int(np.floor(1000 * corr_data['noise'][0, 0])))

        #name = '{:02d}'.format(idx)
        idx=idx+1


        try:
            nSamples=corr_data['dataset'][0].shape[0]
            nPoints=corr_data['dataset']['x1p'][0, 0].shape[1]
        except:
            continue

        x1p=np.zeros((nSamples,nPoints,3))
        x2p=np.zeros((nSamples, nPoints,3))
        labels=np.zeros((nSamples, nPoints),dtype=np.uint8)
        Fn = np.zeros((nSamples, 3, 3))
        tn1 = np.zeros((nSamples, 3, 3))
        tn2 = np.zeros((nSamples, 3, 3))
        for didx in range(nSamples):
            x1p[didx]=corr_data['dataset']['x1p'][0, didx].transpose()
            x2p[didx] = corr_data['dataset']['x2p'][0, didx].transpose()
            labels[didx] = corr_data['dataset']['labels'][0, didx]
            Fn[didx]=corr_data['dataset']['F'][0, didx]

            tn1[didx]=corr_data['dataset']['TN1'][0,didx]
            tn2[didx] = corr_data['dataset']['TN2'][0, didx]


        outlierRatio = np.sum(labels[:]) / np.size(labels)
        if 'noise' in corr_data:
            noise=corr_data['noise'][0, 0]
            print('{} / {:.2f} Outliers. {:d} Samples a {:d} points. Noise: {:.3f}'.format(file, outlierRatio * 100,
                                                                                           nSamples, nPoints, noise))
        else:
            print('{} / {:.2f} Outliers. {:d} Samples a {:d} points.'.format(file,outlierRatio*100,nSamples,nPoints))
            
        data=dict()
        data['x1p']=x1p
        data['x2p'] = x2p
        data['labels'] = labels
        data['Fn'] = Fn
        data['TN1'] = tn1
        data['TN2'] = tn2
        dataset[name]=data
        names.append(name)



    return  dataset,names


def mergeAndShuffleDatasets(dataset):


    x1p = list()
    x2p = list()
    labels = list()
    Fn = list()
    tn1 = list()
    tn2 = list()

    for data in dataset:

        x1p.append(dataset[data]['x1p'].copy())
        x2p.append(dataset[data]['x2p'].copy())
        labels.append(dataset[data]['labels'].copy())
        Fn.append(dataset[data]['Fn'].copy())
        tn1.append(dataset[data]['TN1'].copy())
        tn2.append(dataset[data]['TN2'].copy())

    c = list(zip(x1p,x2p,labels,Fn,tn1,tn2))
    random.shuffle(c)
    x1p,x2p,labels,Fn,tn1,tn2= zip(*c)

    x1p_out=np.concatenate(tuple(x1p),axis=0)
    x2p_out = np.concatenate(tuple(x2p), axis=0)
    labels_out = np.concatenate(tuple(labels), axis=0)
    Fn_out = np.concatenate(tuple(Fn), axis=0)
    tn1_out = np.concatenate(tuple(tn1), axis=0)
    tn2_out = np.concatenate(tuple(tn2), axis=0)



    data = dict()
    data['x1p'] = x1p_out
    data['x2p'] = x2p_out
    data['labels'] = labels_out
    data['Fn'] = Fn_out
    data['TN1'] = tn1_out
    data['TN2'] = tn2_out
    dataset_out=dict()
    dataset_out['merged'] = data
    names_out=list()
    names_out.append('merged')

    return dataset_out, names_out
