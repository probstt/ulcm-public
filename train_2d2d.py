import argparse
import math
import numpy as np
import tensorflow as tf
import socket
import importlib
import os
import sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)
sys.path.append(os.path.join(BASE_DIR, 'models'))
sys.path.append(os.path.join(BASE_DIR, 'utils'))
import provider
import tf_util
import pdb
import os, datetime
import time
from scipy.special import expit
from scipy import io as sio
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', type=int, default=0, help='GPU to use [default: GPU 0]')
parser.add_argument('--model', default='pointnet_maxcon',
                    help='Model name: pointnet_cls or pointnet_cls_basic [default: pointnet_cls]')
parser.add_argument('--log_dir', default='log', help='Log dir [default: log]')
parser.add_argument('--num_point', type=int, default=512, help='Point Number [256/512/1024/2048] [default: 1024]')
parser.add_argument('--max_epoch', type=int, default=250000, help='Epoch to run [default: 250]')
parser.add_argument('--batch_size', type=int, default=16, help='Batch Size during training [default: 32]')
parser.add_argument('--learning_rate', type=float, default=0.0001, help='Initial learning rate [default: 0.001]')
parser.add_argument('--momentum', type=float, default=0.9, help='Initial learning rate [default: 0.9]')
parser.add_argument('--optimizer', default='adam', help='adam or momentum [default: adam]')
parser.add_argument('--decay_step', type=int, default=200000, help='Decay step for lr decay [default: 200000]')
parser.add_argument('--decay_rate', type=float, default=0.7, help='Decay rate for lr decay [default: 0.8]')
parser.add_argument('--supervised', type=bool, default=False, help='Train supervised [default: False]')
parser.add_argument('--inlierLambda', type=float, default=0.1, help='Weight for inlier maximization  [default: 0.1]')
parser.add_argument('--dataset', default='data/fundamental', help='Weight for inlier maximization  [default: data/fundamental]')
FLAGS = parser.parse_args()

BATCH_SIZE = FLAGS.batch_size
NUM_POINT = FLAGS.num_point
MAX_EPOCH = FLAGS.max_epoch
BASE_LEARNING_RATE = FLAGS.learning_rate
GPU_INDEX = FLAGS.gpu
MOMENTUM = FLAGS.momentum
OPTIMIZER = FLAGS.optimizer
DECAY_STEP = FLAGS.decay_step
DECAY_RATE = FLAGS.decay_rate

MODEL = importlib.import_module(FLAGS.model)  # import network module
MODEL_FILE = os.path.join(BASE_DIR, 'models', FLAGS.model + '.py')
LOG_DIR = FLAGS.log_dir
if not os.path.exists(LOG_DIR): os.mkdir(LOG_DIR)
os.system('cp %s %s' % (MODEL_FILE, LOG_DIR))  # bkp of model def
os.system('cp train_2d2d.py %s' % (LOG_DIR))  # bkp of train procedure
LOG_FOUT = open(os.path.join(LOG_DIR, 'log_train.txt'), 'w')
LOG_FOUT.write(str(FLAGS) + '\n')

BN_INIT_DECAY = 0.5
BN_DECAY_DECAY_RATE = 0.5
BN_DECAY_DECAY_STEP = float(DECAY_STEP)
BN_DECAY_CLIP = 0.99

TRAIN_SUPERVISED=FLAGS.supervised
inlierLossLambda=FLAGS.inlierLambda

# Load pre-matched data
def load_data(folder):
    dataset, data_names = provider.modelnet_matches_from_mat_2d2d(folder)
    return dataset

dataset_train=load_data(FLAGS.dataset)
dataset_eval=dataset_train # Train unsupervised, eval on training set


def log_string(out_str):
    LOG_FOUT.write(out_str + '\n')
    LOG_FOUT.flush()
    print(out_str)


def get_learning_rate(batch):
    learning_rate = tf.train.exponential_decay(
        BASE_LEARNING_RATE,  # Base learning rate.
        batch * BATCH_SIZE,  # Current index into the dataset.
        DECAY_STEP,  # Decay step.
        DECAY_RATE,  # Decay rate.
        staircase=True)
    learning_rate = tf.maximum(learning_rate, 0.00001)  # CLIP THE LEARNING RATE!
    return learning_rate


def get_bn_decay(batch):
    bn_momentum = tf.train.exponential_decay(
        BN_INIT_DECAY,
        batch * BATCH_SIZE,
        BN_DECAY_DECAY_STEP,
        BN_DECAY_DECAY_RATE,
        staircase=True)
    bn_decay = tf.minimum(BN_DECAY_CLIP, 1 - bn_momentum)
    return bn_decay


def train():
    with tf.Graph().as_default():
        with tf.device('/device:GPU:' + str(GPU_INDEX)):
            pointclouds_pl, labels_pl, mask_pl, vander_pl,\
                fund_pl,motiontype_pl= MODEL.placeholder_inputs_2d2d(BATCH_SIZE, NUM_POINT,\
                                                                                     inputDim=4,\
                                                                                     vanderDim=9)
            is_training_pl = tf.placeholder(tf.bool, shape=())
            print(is_training_pl)

            # Note the global_step=batch parameter to minimize.
            # That tells the optimizer to helpfully increment the 'batch' parameter for you every time it trains.
            batch = tf.Variable(0)
            bn_decay = get_bn_decay(batch)
            # tf.summary.scalar('bn_decay', bn_decay)

            # Get model and loss
            pred, end_points = MODEL.get_model(pointclouds_pl, is_training_pl, bn_decay=bn_decay,inputDim=4)
            loss = MODEL.get_loss_2d2d(pred, labels_pl, mask_pl, vander_pl, supervised=TRAIN_SUPERVISED, inlierLossLambda=inlierLossLambda,doHomography=False)
            tf.summary.scalar('loss', loss)

            # accuracy
            correctPred = tf.equal(tf.cast(tf.greater_equal(tf.sigmoid(pred), 0.5), tf.int64), tf.to_int64(labels_pl))
            correctPred = tf.cast(correctPred, tf.float32)
            correct = correctPred * mask_pl
            accuracy = tf.reduce_sum(tf.cast(correct, tf.float32)) / tf.reduce_sum(mask_pl)
            tf.summary.scalar('accuracy', accuracy)

            # true inlier rate
            gtInliers = tf.cast(tf.less(labels_pl, 0.5), tf.float32)
            correctInliers = correctPred * gtInliers
            correct_inliers = tf.reduce_sum(tf.cast(correctInliers, tf.float32)) / tf.reduce_sum(gtInliers)
            tf.summary.scalar('a_inlier_detection_rate', correct_inliers)

            # detected outliers rate
            gtOutliers = tf.cast(tf.greater_equal(labels_pl, 0.5), tf.float32)
            correctly_found_outliers = correctPred * gtOutliers
            correctly_found_outliers = tf.reduce_sum(tf.cast(correctly_found_outliers, tf.float32)) / tf.reduce_sum(
                gtOutliers)
            tf.summary.scalar('a_outlier_detection_rate', correctly_found_outliers)

            # Get training operator
            learning_rate = get_learning_rate(batch)
            tf.summary.scalar('learning_rate', learning_rate)
            if OPTIMIZER == 'momentum':
                optimizer = tf.train.MomentumOptimizer(learning_rate, momentum=MOMENTUM)
            elif OPTIMIZER == 'adam':
                optimizer = tf.train.AdamOptimizer(learning_rate)
            train_op = optimizer.minimize(loss, global_step=batch)

            # Add ops to save and restore all the variables.
            saver = tf.train.Saver()

        # Create a session
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.allow_soft_placement = True
        config.log_device_placement = False
        sess = tf.Session(config=config)

        # Add summary writers
        # merged = tf.merge_all_summaries()
        merged = tf.summary.merge_all()
        stamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        folderName = os.path.join(os.path.join(LOG_DIR, 'exp'), stamp + '_train')
        os.makedirs(folderName)
        train_writer = tf.summary.FileWriter(folderName,
                                             sess.graph)
        folderName = os.path.join(os.path.join(LOG_DIR, 'exp'), stamp + '_test')
        os.makedirs(folderName)
        test_writer = tf.summary.FileWriter(folderName)

        # Init variables
        init = tf.global_variables_initializer()
        # To fix the bug introduced in TF 0.12.1 as in
        # http://stackoverflow.com/questions/41543774/invalidargumenterror-for-tensor-bool-tensorflow-0-12-1
        # sess.run(init)
        sess.run(init, {is_training_pl: True})

        ops = {'pointclouds_pl': pointclouds_pl,
               'labels_pl': labels_pl,
               'mask_pl': mask_pl,
               'vander_pl': vander_pl,
               'fund_pl':fund_pl,
               'motiontype_pl':motiontype_pl,
               'is_training_pl': is_training_pl,
               'pred': pred,
               'loss': loss,
               'train_op': train_op,
               'merged': merged,
               'step': batch}

        ###############
        # Train loop  #
        ###############
        log_string(f'Training parameters: supervision={TRAIN_SUPERVISED}, inlierLambda={inlierLossLambda}')

        # eval before training
        eval_one_epoch_from_mat(dataset_eval, sess, ops, test_writer,useBatchStatistics=False)

        for epoch in range(MAX_EPOCH):
           
    
            log_string('**** TRAIN EPOCH %03d ****' % (epoch))
            train_one_epoch_from_mat(dataset_train, sess, ops, train_writer)


            #Evaluate and save the variables to disk.
            if epoch>0 and epoch % 10 == 0:
                log_string('**** EVAL EPOCH %03d ****' % (epoch))
                sys.stdout.flush()
                eval_one_epoch_from_mat(dataset_eval, sess, ops, test_writer,useBatchStatistics=False)

                save_path = saver.save(sess, os.path.join(LOG_DIR, "model.ckpt"))
                log_string("Model saved in file: %s" % save_path)


def train_one_epoch_from_mat(dataset_in, sess, ops, train_writer):
    """ ops: dict mapping from string to tf ops """
    is_training = True
    total_correct = 0
    total_seen = 0
    loss_sum = 0
    total_batches = 0

    start_time = time.time()
    dataset,_=provider.mergeAndShuffleDatasets(dataset_in)

    current_data, current_targets, current_mask, current_vander = provider.prepare_2d2d_prematched(
        dataset['merged'])
    print(f'Dataset length {len(dataset)}.')

    num_batches = current_data.shape[0] // BATCH_SIZE

    for batch_idx in range(num_batches):
        start_idx = batch_idx * BATCH_SIZE
        end_idx = (batch_idx + 1) * BATCH_SIZE

        batch_data = current_data[start_idx:end_idx, :, :]
        targets_data = current_targets[start_idx:end_idx, :]
        mask_data = current_mask[start_idx:end_idx, :]
        vander_data = current_vander[start_idx:end_idx, :]


        feed_dict = {ops['pointclouds_pl']: batch_data,
                     ops['labels_pl']: targets_data,
                     ops['mask_pl']: mask_data,
                     ops['vander_pl']: vander_data,
                     ops['is_training_pl']: is_training, }
        summary, step, _, loss_val, pred_val = sess.run([ops['merged'], ops['step'],
                                                         ops['train_op'], ops['loss'], ops['pred']],
                                                        feed_dict=feed_dict)

        pred_class = expit(pred_val) > 0.5
        correct = np.sum(np.float32(pred_class == (targets_data > 0.5)) * mask_data)
        nActivePts = np.sum(mask_data.reshape(-1))

        # sio.savemat('debug.mat', {'batch': batch_data, 'targets': current_targets, 'pred': pred_class})

        total_correct += correct
        total_seen += nActivePts
        loss_sum += loss_val

        train_writer.add_summary(summary, step)

        acc = correct / float(nActivePts)
        log_string(f'{batch_idx}/{num_batches}: mean acc={acc:.3f}')

    total_batches = total_batches + num_batches

    log_string('mean loss: %f' % (loss_sum / float(num_batches)))
    acc = total_correct / float(total_seen)
    log_string('total acc: %f' % acc)

    total_time = time.time() - start_time
    batch_time = total_time / float(total_batches)
    sample_time = batch_time / float(BATCH_SIZE)
    log_string('avg time per sample: {:.6f}'.format(sample_time))
    log_string('avg time per batch: {:.6f}'.format(batch_time))


def eval_one_epoch_from_mat(dataset, sess, ops, test_writer,useBatchStatistics):
    """ ops: dict mapping from string to tf ops """
    is_training = useBatchStatistics
    total_correct = 0
    total_seen = 0
    loss_sum = 0
    total_batches = 0

    start_time = time.time()
    print(f'Dataset length {len(dataset)}.')

    for fn,data in enumerate(dataset):
        log_string('----' + str(data) + '-----')

        current_data, current_targets, current_mask, current_vander = provider.prepare_2d2d_prematched(
            dataset[data])

        current_results = np.zeros(current_targets.shape, np.float32)

        num_batches = current_data.shape[0] // BATCH_SIZE

        for batch_idx in range(num_batches):
            start_idx = batch_idx * BATCH_SIZE
            end_idx = (batch_idx + 1) * BATCH_SIZE

            batch_data = current_data[start_idx:end_idx, :, :]
            targets_data = current_targets[start_idx:end_idx, :]
            mask_data = current_mask[start_idx:end_idx, :]
            vander_data = current_vander[start_idx:end_idx, :]

            # pdb.set_trace()
            feed_dict = {ops['pointclouds_pl']: batch_data,
                         ops['labels_pl']: targets_data,
                         ops['mask_pl']: mask_data,
                         ops['vander_pl']: vander_data,
                         ops['is_training_pl']: is_training}
            summary, step, loss_val, pred_val = sess.run([ops['merged'], ops['step'],
                                                          ops['loss'], ops['pred']], feed_dict=feed_dict)

            current_results[start_idx:end_idx, :] = expit(pred_val)

            pred_class = expit(pred_val) > 0.5
            correct = np.sum(np.float32(pred_class == (targets_data > 0.5)) * mask_data)
            nActivePts = np.sum(mask_data.reshape(-1))

            # pdb.set_trace()

            total_correct += correct
            total_seen += nActivePts
            loss_sum += loss_val

            test_writer.add_summary(summary, step)

            acc = correct / float(nActivePts)
            log_string(f'{batch_idx}/{num_batches}: mean acc={acc:.3f}')

        total_batches = total_batches + num_batches

        # export results to mat
        # sio.savemat('debug.mat', {'batch': current_data, 'targets': current_targets, 'pred': current_results})
        dir = '{}/output/eval_epoch_{}/'.format(LOG_DIR, step)
        try:
            os.makedirs(dir)
        except:
            pass
        fname = os.path.join(dir, 'results_lsac_{}.mat'.format(fn))
        sio.savemat(fname, {'batch': current_data, 'targets': current_targets, 'pred': current_results})

    log_string('eval mean loss: %f' % (loss_sum / float(total_seen)))
    acc = total_correct / float(total_seen)
    log_string('eval total acc: %f' % acc)

    # pdb.set_trace()
    total_time = time.time() - start_time
    batch_time = total_time / float(total_batches)
    sample_time = batch_time / float(BATCH_SIZE)
    log_string('avg time per sample: {:.6f}'.format(sample_time))
    log_string('avg time per batch: {:.6f}'.format(batch_time))

if __name__ == "__main__":
    train()
    LOG_FOUT.close()
